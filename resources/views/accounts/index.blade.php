{{-- {{"this is index page"}} --}}


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Select2 Jquery library</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    {{-- select2 link --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


</head>
<body>

    {{-- @dd($data) --}}
    
<h1> Select2 jQuery library</h1>    

{{-- <button class="btn btn-primary">This</button> --}}

<select name="" id="" class="select2">
    <option value="">Select your City</option>
<optgroup label="India">
    <option value="">Mumbai</option>
    <option value="">Pune</option>
    <option value="">Hyderabad</option>
    <option value="">Bhiwandi</option>
    <option value="">Malad</option>
    <option value="">Bhavnagar</option>
    <option value="">Bihar</option>
</optgroup>
    {{-- <p>Out of Inida</p> --}}

    <optgroup label="Out of India">
    <option value="">Maharashtra</option>
    <option value="">Australia</option>
    <option value="">London</option>
    <option value="">United Kingdom</option>
    <option value="">France</option>
    <option value="">Italy</option>
    
    </optgroup>
</select>


<h3>Name- placeholder</h3>
<select class="select2-name">
    <option value="">Select Your name</option>

@foreach ($data as $item)

<option value="{{$item->id}}">{{$item->name}}</option>
{{-- <option value="{{$item->id}}">{{$item->branch}}</option> --}}

    
@endforeach
</select>


<h4>Multi select - Branch</h4>
<select class="select2" multiple="multiple" style="width: 75%">
    <option value="">Select Your branch</option>
    @foreach ($data as $item)
    
    {{-- <option value="{{$item->id}}">{{$item->name}}</option> --}}
    <option value="{{$item->id}}">{{$item->branch}}</option>
    
        
    @endforeach
    </select>


<script>
$(document).ready(function() {


  


    $('.select2').select2({
        closeOnSelect: false,
        theme: "classic",
        tags: false
    });
   


    //with place holder
    $('.select2-name').select2({
        placeholder: 'Select an option'
    });

    // $('.select2').select2('close');


});
</script>
</body>
</html>